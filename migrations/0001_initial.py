# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='PageKeywords',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('page_id', models.CharField(max_length=255)),
                ('rank', models.IntegerField()),
                ('keyword', models.CharField(max_length=255)),
            ],
        ),
    ]
