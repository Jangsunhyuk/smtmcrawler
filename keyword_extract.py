from src.csv_loader import CsvLoader
from src.user_crawler import UserCrawler
import sys

uc = UserCrawler()

uc.login(sys.argv[1],sys.argv[2])

cl = CsvLoader()

for ud in cl.load("src/user_data.csv"):
	uc.get_profiles(ud)

print("분석 결과!!!!!")
for keyword in uc.km.get_report():
	print(keyword)

uc.km.clean()
uc.get_demoinfo()

#release UserCrawler!
del uc
