from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$',views.home, name='home'),
	url(r'^crawler/',views.keyword_extract, name='keyword_extract'),
]
