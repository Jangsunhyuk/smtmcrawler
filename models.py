from django.db import models

# Create your models here.

# 페이지 분석 결과 저장하는 모델.
class PageKeywords(models.Model):
	page_id = models.CharField(max_length=255)
	rank = models.IntegerField()
	keyword = models.CharField(max_length=255)
	
