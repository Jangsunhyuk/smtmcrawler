from django.shortcuts import render
from .src.extractor import Extractor
from django.http import HttpResponse
from django.http import JsonResponse
from multiprocessing import Process, Queue

#disable csrf
from django.views.decorators.csrf import csrf_exempt

import json
@csrf_exempt
def keyword_extract(request):
	if request.method == 'POST':
		req_json = request.body
		
		#POST로 받은 json을 받아 items에 넣음
		items = json.loads(request.body.decode("utf-8"))
		
		project_id = None
		if "project_id" in items:
			project_id = items["project_id"]
		#l : phone number lists
		l = None

		if "items" in items:
			l = [ item["phone"] for item in items["items"] if "phone" in item ]

		if l == None:
			return HttpResponse("Can't find items")
	
		try:
			if project_id != None:
				et = Extractor(project_id, "wkdtjsgur100@nate.com", "a!!^&@%!1", l)
				et.start()
		
		except Exception as e:
			print(e)
		return JsonResponse({"success":True})
	
	return JsonResponse({"success":False})

def home(request):
	return HttpResponse("HOME")

