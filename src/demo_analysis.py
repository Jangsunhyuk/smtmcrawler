
"""
	인구 통계학 정보를 분석한다.
"""
class DemoAnalysis:
	def __init__(self):
		self.demo_infos = dict(dict())

	def add_demoinfo(self, key, value):
		if key in self.demo_infos:
			if value in self.demo_infos[key]:
				self.demo_infos[key][value] += 1
			else:
				self.demo_infos[key][value] = 1
		else:
			self.demo_infos[key] = { value : 1 }		

	def get_demoinfo(self):
		return self.demo_infos
