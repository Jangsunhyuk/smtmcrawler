from pyvirtualdisplay import Display
from selenium import webdriver

display = Display(visible=0, size=(1024, 768))
display.start()

driver = webdriver.Chrome('/usr/local/chromedriver')
driver.get("http://www.example.com")
#driver.get('https://www.facebook.com/')
print(driver.page_source)

driver.close()
display.stop()
