# -*- coding: utf-8 -*- 

#from .models import ContentInfo
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException

"""
USAGE : python page_crawler.py [arg1] [arg2]
arg1 : crawling page count
arg2 : crawling post count 
"""

import json
#for argv
import sys
#require: sudo pip install requests
import requests

from text_rank import TextRank
#from pyvirtualdisplay import Display

"""
    PageAnalysis class -- 

    bigfoot9에 있는 페이지들을 분석해 분석 결과를 DB에 저장한다.
"""
class PageCrawler:
    def __init__(self):
        pass

    def crawl_start(self):
        print("display started")
      #  self.display = Display(visible=0, size=(800, 600))
       # self.display.start()
        """	
        options = webdriver.ChromeOptions()

        options.binary_location = '/usr/bin/chromium-browser'#All the arguments added for chromium to work on selenium
        options.add_argument("--no-sandbox") #This make Chromium reachable
        options.add_argument("--no-default-browser-check") #Overrides default choices
        options.add_argument("--no-first-run")
        options.add_argument("--disable-default-apps")
        """
        self.driver = webdriver.Chrome('/usr/local/chromedriver')

        client_id = "1038580469574247"
        client_secret = "30d64387b1404864c8ef5c923950d1d9"
        
        self.get_fb_token(client_id,client_secret)

        print("loading page url")
        self.write_page_posts(100)

        self.driver.close()
        #self.display.stop()

    #return : json dictionary response
    #warning : post_limit cannot exceed 100
    def get_all_posts(self, url, post_limit = 15):
        _params = { 'limit' : post_limit, 'access_token' : self.atkn }
        file = requests.get(url,params = _params)
        dict = json.loads(file.text,encoding='utf-8')

        return dict

    #write on DB and terminal from pages json data
    def write_from_pages_json(self,pages_json):
        for item in pages_json:
            page_id = item.get_attribute("href").split("/")[-1]
            
            #page_id duplication check
            #if ContentInfo.objects.filter(from_id=page_id).count() != 0:
                #continue

            url = "https://graph.facebook.com/" + page_id + "/posts"
            dict = self.get_all_posts(url,100)

            tr = TextRank()

            if "data" in dict:
                for i in range(len(dict["data"])):
                    if "message" in dict["data"][i]:
                        if dict["data"][i]["message"]:
                            data = dict["data"][i]["message"]

                            tr.str_mapping_to_graph(data)

            print(page_id + "'s post is crawled & saved. ")

            i = 0
            for key,value in tr.extract_keyword_from_graph():
                print("key : " + key + " value : " + str(value))
                i += 1
                #상위 30개 출력
                if i == 30: 
                    break

    #get facebook token
    def get_fb_token(self,app_id, app_secret):
        payload = {'grant_type': 'client_credentials', 'client_id': app_id, 'client_secret': app_secret}
        file = requests.post('https://graph.facebook.com/oauth/access_token?', params = payload)
        #print file.text #to test what the FB api responded with    
        result = file.text.split("=")[1]
        #print file.text #to test the TOKEN
        self.atkn = result

    # write page posts from big foot 9
    def write_page_posts(self,page_num):
        # get All category
        url = "http://bigfoot9.com/today-page?cc=KR&cat=All&pgo=fans"

        self.driver.get(url)

        t=0

        while t < page_num:
            try:
                more_button = self.driver.find_element_by_xpath("//input[@class='btn-col-exp newbtn blue core listmore']")
                more_button.click()

                t += 10
            except Exception:
                pass
 
        print("crawled a tag....!")
        texts_json = self.driver.find_elements_by_xpath("//div[@class='pic']/a")
        self.write_from_pages_json(texts_json)

#########################

pc = PageCrawler()

pc.crawl_start()
