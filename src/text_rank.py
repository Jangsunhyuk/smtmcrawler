import networkx as nx
#형태소 분석
from konlpy.tag import Twitter

"""
TextRank --

  String을 분석해 키워드를 추출 해준다.

USAGE ---

from text_rank import TextRank

tr = TextRank()

f = open("twit_1.csv","r")
tr.str_mapping_to_graph(f.read())

f2 = open("twit_2.csv","r")
tr.str_mapping_to_graph(f2.read())

for key,value in tr.extract_keyword_from_graph():
	print("key : " + key + "value : " + str(value))

"""

class TextRank:
	def __init__(self):
		#for morph_analysis
		self.twitter = Twitter()
		#for textrank
		self.G = nx.Graph()

		#주어진 텍스트의 string이 너무 길면 너무 느려지게 되니, 어느정도의 제한을 걸어둬야 한다.
		self.text_string_max = 400

	#string이 주어지면 형태소를 분석, 필터링해 분석 결과를 리스트로 반환한다.(bigram도 추가)
	def __morph_analysis(self, string):
		n = len(string)

		if len(string) > self.text_string_max:
			n = self.text_string_max

		tuple_list = self.twitter.pos(string[0:n], norm=True, stem=True)

		#morph unigram
		l_uni = [ morph for morph, morph_type in tuple_list if morph_type not in ['Josa','Punctuation','Foreign','Alpha','Number'] ]
		#morph bigram
		bi_uni = [ l_uni[i]+l_uni[i+1] for i in range(len(l_uni)-1) ]

		return l_uni + bi_uni

	#pagerank를 하기 위해 형태소의 리스트들을 graph로 맵핑시킨다.
	def __morph_list_to_graph(self, morph_list):
		length = len(morph_list)
		for i in range(0,length):
			for j in range(i+1,length):
				#만약 morph_list[i], morph_list[j]에 대한 weight값이 이미 존재하면
				if morph_list[i] in self.G and morph_list[j] in self.G[morph_list[i]]:
					_weight = self.G[morph_list[i]][morph_list[j]]['weight'] + 1
				else:
					_weight = 1

				self.G.add_edge(morph_list[i],morph_list[j],weight = _weight)

	#string을 받으면 형태소 분석/weight 계산 후 graph에 매핑한다.
	#s_target 분석 할 string.
	def str_mapping_to_graph(self,s_target):
		morph_list = self.__morph_analysis(s_target)
		self.__morph_list_to_graph( morph_list )

	#generator, self.G에 따라서 키워드와 값을 yield 한다.
	# warning : clear graph를 해야한다.
	def extract_keyword_from_graph(self, _max_iter=300, n=100):
		result = nx.pagerank(self.G, max_iter=_max_iter)
		count = 1
		
		l = []
		for w in sorted(result, key=result.get, reverse=True):
			l.append((w, result[w]))
			if count >= n:
				break
			count += 1

		return l
	#clean Graph
	def clear_graph(self):
		self.G.clear()


####################
