from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

from datetime import datetime

from .demo_analysis import DemoAnalysis
from .page_analysis import PageAnalysis

#텍스트 랭크 분석
from .text_rank import TextRank

#key match
from .keymatch.keymatch import Keymatch
#for CLI
from pyvirtualdisplay import Display

"""
	포스트 제한 개수 : 50
	그룹/페이지 가져오는 개수 : 50
"""

class UserCrawler:
	def __init__(self):
		#######
		self.display = Display(visible=0, size=(800, 600))
		self.display.start()
        	#######
        
		options = webdriver.ChromeOptions()
		
		options.add_argument("--no-sandbox")
		options.add_argument("--disable-default-apps")
		options.add_argument("--no-default-browser-check")
		options.add_argument("--disable-notifications")

		self.driver = webdriver.Chrome('/usr/local/chromedriver',chrome_options=options)
		
		print("facebook page 접속중..")
		self.driver.get("https://www.facebook.com")

		self.logined = False

		self.demo_info = DemoAnalysis()
		self.tr = TextRank()
		self.pa = PageAnalysis()
		self.km = Keymatch()
			
		self.all_user_count = 0
		self.vaild_user_count = 0
		#로딩을 기다리는 시간. 이 시간이 지나면 포스트가 더 이상 없는 것으로 판단한다.
		self.loading_wait_time = 5

	def __del__(self):
		self.driver.quit()
		self.display.stop()

	#user_data를 기반으로 사용자의 타임라인을 찾아 접속한다. 찾을 수 없으면 찾을수 없다고 출력.
	def __search_timeline(self,user_data):
		print(user_data + "의 신상을 털고 있습니다.")
		timeline_url = "https://www.facebook.com/public?query=" + user_data
		#facebook 존재여부 검사
		self.driver.get(timeline_url)
		try:
			a_xpath = "//div[@class='clearfix']//a[@tabindex='-1']"

			a_element = WebDriverWait(self.driver, 2).until(EC.element_to_be_clickable((By.XPATH, a_xpath)))	
			a_element.click()
			
			#유효한 사용자 수 증가.
			self.vaild_user_count += 1	
			return True
		except Exception as e:
			print("Facebook을 찾을 수 없습니다..")
			return False

	#t 시간 동안 스크롤 한다.
	def __scroll_with_time(self, t):
		start_time = datetime.now()

		while True:
			if (datetime.now() - start_time).seconds > t:
				break

			self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

	#self.base_url과 target을 바탕으로 url을 만들어 준다.
	def __make_url(self, target):
		#url이 ?id=12951025으로 끝날 때와 /Jang으로 끝날 때를 구분한다.
		if '?' in self.base_url:
			token = '&sk='
		else:
			token = '?sk='

		return self.base_url + token + target

	#인구 통계학 정보의 최소 단위 를 yield
	def __crawl_demo_unit(self, url):
		self.driver.get( url )
		
		try:
			work_elems = self.driver.find_elements_by_xpath("//div[@class='_6a']")

			for work_elem in work_elems:
				try:
					main = work_elem.find_element_by_xpath(".//a")
					try:
						sub = work_elem.find_element_by_xpath(".//div[@class='fsm fwn fcg']")
						yield main.text, sub.text

					except NoSuchElementException:
						yield main.text, ""
				except NoSuchElementException:
					yield "", ""
		except Exception as e:
			print("인구 통계학 정보 : 알수 없는 에러")
			print(e)
	#인구통계학 정보 크롤해 yield
	def __crawl_demoinfo(self):

		base = self.__make_url("about") + "&section="
		###### 경력 및 학력 크롤
		for company, sub in self.__crawl_demo_unit( base + "education"):
			if company is not "":
				print("직장/학교은 : " + company)

				self.demo_info.add_demoinfo("직장/학교",company)
			if sub is not "":
				print("직업/학력은 : " + sub)
				for item in sub.split("·"):
					if "년" not in item:
						self.demo_info.add_demoinfo("직업/학력",item.strip())

		###### 거주지 크롤
		for region, sub in self.__crawl_demo_unit( base + "living"):
			if region != "":
				print("거주지는 : " + region)
				self.demo_info.add_demoinfo("거주지", region)

				if sub != "":
					print("부가 정보는 : "+ sub)


		###### 기본정보 크롤
		self.driver.get( base + "contact-info" )
		try:
			info_divs = self.driver.find_elements_by_xpath("//div[@class='clearfix _ikh']")

			for info_div in info_divs:
				title = info_div.find_element_by_xpath(".//span[@class='_50f8 _50f4 _5kx5']").text
				info = info_div.find_element_by_xpath(".//div[@class='_4bl7 _pt5']").text
				print(title + "은 " + info)

				if "생일" in title:
					self.demo_info.add_demoinfo(title,info.split(" ")[0])
				else:
					self.demo_info.add_demoinfo(title,info)

		except NoSuchElementException:
			print("기본 정보가 없습니다.")

		###### 가족관계 크롤
		self.driver.get(base + "relationship")
		try:
			love_div = self.driver.find_element_by_xpath("//div[@class='_42ef']")
			
			val = ""
			if "없음" in love_div.text:
				print("연애/기혼/싱글 정보가 없습니다.")
				val = "없음"
			elif "연애" in love_div.text:
				print("연애 중입니다")
				val = "연애"
			elif "결혼" in love_div.text or "기혼" in love_div.text:
				print("기혼자 입니다")
				val = "기혼"
			elif "싱글" in love_div.text:
				print("싱글 입니다.")
				val = "싱글"
			else:
				print(love_div.text+"입니다.")
				val = love_div.text

			self.demo_info.add_demoinfo("relationship",val)

		except NoSuchElementException:
			print("relationship 정보를 찾을 수 없음.")	

	#href를 다듬어서 id를 만들고 페이지 분석기에 돌린다. hType에는 group, page가 있다.
	def __trim_and_analyze_page(self,href,hType):
		result_id = None
		result_l = None

		if hType == "groups":
			result_id = href.split("/")[4]
			result_l = self.pa.page_analyze(result_id,"group")

		if hType == "likes":
			#일반적인 페이지가 아닌 경우엔 분석 안돌림

			if "/pages/" in href:
				result_id = href.split("/")[5]
			else:
				result_id = href.split("/")[3]

			if "%" in result_id:
				result_id = result_id.split("-")[-1]
			
			if "profile_browser" in result_id:
				result_id = result_id.split("?")[0]
					
			result_l = self.pa.page_analyze(result_id,"page")
			
		if result_l != None and result_id != None:
			print("페이지/그룹 분석결과 ------")
			for re in self.km.put_page_new(self.user_data,result_l):
				print(re)

	#좋아요 정보를 크롤해서 좋아요 한 페이지의 id를 출력
	def __crawl_like_pages(self):
		print("좋아요 정보를 가져오는 중입니다")

		target_l = [ \
		 ("likes","pagelet_timeline_medley_likes",".//li[@class='_5rz _5k3a _5rz3 _153f']",".//div[@class='clearfix _5t4y _5qo4']//a[@tabindex='-1']") , \
		 ("groups","pagelet_timeline_medley_groups",".//li[@class='_153f']",".//div[@class='mbs fwb']//a")]

		try:
			for target, parent_id, component_path, xpath in target_l:
				url = self.__make_url(target)
				self.driver.get(url)

				#원래의 url로 redirection되면 공개가 안된것으로 처리.
				if self.driver.current_url == self.base_url:
					print("사용자가 해당 내용을 공개하지 않았습니다")
					continue

				parent_div = self.driver.find_element_by_id(parent_id)

				max_crawl_num = 50

				last_idx = 0

				while True:
					#로딩이 완료 될때 까지 wait.
					bTimeOut = False

					start_time = datetime.now()
					while True:
						components = parent_div.find_elements_by_xpath(component_path)

						if last_idx != len(components):
							break

						if (datetime.now() - start_time).seconds > self.loading_wait_time:
							bTimeOut = True
							break

					if len(components) > max_crawl_num or bTimeOut == True:
						break
					else:
						last_idx = len(components)
						self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

				a_elements = parent_div.find_elements_by_xpath(xpath)

				print(target + "'s info ----- ")

				i = 0
				for a_elem in a_elements:
					href = a_elem.get_attribute('href')
					self.__trim_and_analyze_page(href,target)
					if i >= 3:
						break
					i += 1
		except Exception as e:
			print("알수 없는 에러")
			print(e)

	#text가 생일과 관련된 글이면 True, 아니면 False를 반환
	def __check_with_birthday(self,text):
		except_words = ["생일","생신","Birth","BIRTH","birth","생축"]

		for except_word in except_words:
			if except_word in text:
				return True

		return False

	#posts안에 있는 모든 내용들을 출력
	def __print_all_inner_data(self,posts):
		print("************ 크롤 된 포스트 개수 : "+ str(len(posts)) + "개 ***************")
		for post in posts:
			#메인 메세지 긁어서 보여주기
			try:
				post_messages = post.find_elements_by_xpath(".//div[@class='_5pbx userContent']")
				
				for post_message in post_messages:
					#생일과 관련없으면..
					if self.__check_with_birthday(post_message.text) == False:
						filtered_message = post_message.text.replace("번역 보기","").replace("더 보기","")
						print(filtered_message)
						self.tr.str_mapping_to_graph(filtered_message)

			except NoSuchElementException:
				pass

			#공유 메세지 긁어서 보여주기
			try:
				shared_messages = post.find_elements_by_xpath(".//div[@class='mtm _5pco']")

				for shared_message in shared_messages:
					filtered_message = shared_message.text.replace("번역 보기","").replace("더 보기","")
					print(filtered_message)
					self.tr.str_mapping_to_graph(filtered_message)						

			except NoSuchElementException:
				pass

	#포스트 털기
	def __crawl_posts(self, max_post_num=50):
		print("포스트를 긁어오는 중입니다..")

		if self.driver.current_url != self.base_url:
			self.driver.get(self.base_url)

		#포스트 내용을 긁어옴.
		last_idx = 0

		while True:
			try:
				if self.base_url not in self.driver.current_url:
					print("redirecting problem")
					break

				#loading_wait_time초 동안 게시물이 로딩되지 않으면 포스트가 존재하지 않는 것으로 판단.
				WebDriverWait(self.driver, self.loading_wait_time).until(EC.presence_of_element_located((By.XPATH, "//div[@class='userContentWrapper _5pcr']")))
				
				timer = datetime.now()

				bTimeOut = False
	
				posts = []

				
				while True:
					posts = self.driver.find_elements_by_xpath("//div[@class='_1dwg _1w_m']")
					
					if last_idx != len(posts):
						break

					#로딩이 될때 까지 기다림. loading_wait_time 초가 지나면 timeout
					if (datetime.now() - timer).seconds > self.loading_wait_time:
						print("***************** 게시물이 더이상 없습니다! *********************")
						bTimeOut = True
						break

				for i in range(last_idx,len(posts)):
					post = posts[i]

					#더 보기 있을 시 버튼 누르기
					try:
						elem_see_more = post.find_element_by_xpath(".//a[@class='see_more_link']")
				
						print("더 보기를 발견 했습니다! 클릭 합니다..***")
						webdriver.ActionChains(self.driver).move_to_element(elem_see_more).click().perform()

					except Exception: 
						pass	

				#긁은 posts의 개수가 max_post_num을 넘거나 더 이상 스크롤 할 수 없으면 포스트 글 전부를 출력하고 종료
				if len(posts) >= max_post_num or bTimeOut == True :
					#posts 안의 모든 내용을 print
					self.__print_all_inner_data(posts)
					break #break while..

				#그렇지 않으면 스크롤
				else:
					last_idx = len(posts)
					#for loading more posts..
					self.driver.execute_script("window.scrollTo(0, document.body.scrollHeight);")

			except Exception as e:
				print(e)
				print("포스트가 존재하지 않습니다.")
				break

	def login(self, auth_id,auth_pw):
		print("로그인 중입니다...")
		
		elem = self.driver.find_element_by_id("email")
		elem.send_keys(auth_id)
		elem = self.driver.find_element_by_id("pass")
		elem.send_keys(auth_pw)
		elem.submit()

		self.logined = True
		print("현재 url : "+self.driver.current_url)

	def get_profiles(self, user_data):
		self.user_data = user_data

		if self.logined == False:
			print("로그인이 안 되어있습니다.")
			exit()

		if self.__search_timeline(user_data):
			self.base_url = self.driver.current_url

			self.__crawl_posts()
			self.analyze_timeline()

			self.__crawl_demoinfo()
		#	self.__crawl_like_pages()
			
			print(str(self.all_user_count) + "번째 고객의 분석 완료-----")
					
		self.all_user_count += 1

	def analyze_timeline(self):
		l = self.tr.extract_keyword_from_graph(n=100)
		
		print("timeline 분석 결과 --- ")
		for result in self.km.put_user_timeline(self.user_data,l):
			print(result)
		
		self.tr.clear_graph()
	#크롤 한 정보들을 바탕으로 분석한 인구통계학 정보들을 출력한다.
	def get_demoinfo(self):
		return self.demo_info.get_demoinfo()


