from .user_crawler import UserCrawler
from .reach_estimate import ReachEstimate

import threading

import requests
import json
from .csv_loader import CsvLoader

class Extractor(threading.Thread):
	#l_phone_num : 폰번호들의 리스트 
	def __init__(self, project_id, login_id, login_pw, l_phone_num):
		threading.Thread.__init__(self)
		
		self.project_id = project_id
		self.login_id = login_id
		self.login_pw = login_pw
		self.pn = l_phone_num
	def success_request(self,req_json):	
		url = "http://api.lookalike.co.kr/v1/project/"+ str(self.project_id) + "/analyze"
		print(url)
		
		try:
			headers = {'Content-type':'application/json','Accept':'text/plain'}
			request = requests.post(url,data=json.dumps(req_json),headers=headers)
			print("getting request")
			print(request)
			print("Error printing Request")
		except Exception as e:
			print(e)
	def run(self):
		uc = UserCrawler()
		
		uc.login(self.login_id,self.login_pw)
		
		
		cl = CsvLoader()

		#for phone_num in cl.load("src/user_data.csv"):
		i = 0
		for phone_num in self.pn:
			uc.get_profiles(phone_num)

			if i >= 1:	
				break
			i += 1

		demo_info = uc.get_demoinfo()
		raw_tuple = uc.km.get_report()		
		raw_report = raw_tuple[1]
		
		re = ReachEstimate()

		#순서대로 [ 키워드, SMTM Score, 분석 대상에서 발견 수, 도달범위]
		report = [ [ keyword, l_info[0],  l_info[1], re.get_reach(keyword) ] for keyword, l_info in raw_report ]
		uc.km.clean()	
		
		req_json = {
			"all_user_count" : uc.all_user_count, #전체 크롤링 유저 수
			"vaild_user_count" : uc.vaild_user_count, # 유효한 유저 수
			"report": report, # 최종 상위 키워드 정보
			"demo_info": demo_info, # 인구통계학 정보				
		}
		print(req_json)

		self.success_request(req_json)
		del uc
		print("성공적으로 종료 되었습니다")
	
