# gensim for using word2vec
import gensim;


# In[2]:

# for sorting dictionary by values
from operator import itemgetter


# ### Class

# In[3]:

class Keymatch:
    def __init__(self):
        #self.model = gensim.models.Word2Vec.load('lookalike_w2v_model')
        self.model = gensim.models.Word2Vec.load('src/keymatch/lookalike_namu_model3')
        #print('투자은행' in self.model)
        
        keylistfile = open('src/keymatch/keywords.txt', 'r', encoding='utf-8')
        
        self.keyvectors = {}
        while True:
            line = keylistfile.readline().replace("\n", "")
            if not line: break
            if line in self.model:
                self.keyvectors[line] = self.model[line]
                
        self.usrdatum = {}
                
    def put_user_timeline(self, userid, usrtuplelist):
        tmlscore = {}
        for usrtuple in usrtuplelist:
            for onekey in self.keyvectors.keys():
                if usrtuple[0] in self.model:
                    similar = self.model.similarity(usrtuple[0], onekey)
                    
                    if(similar>0.6):
                        #if onekey == '연애':
                            #print('연애 -- '+usrtuple[0]+' -- '+str(similar))
                        if onekey in tmlscore.keys() : 
                            tmlscore[onekey] += similar * usrtuple[1]
                        else :
                            tmlscore[onekey] = similar * usrtuple[1]
        
        #print(tmlscore)
        
        if (userid in self.usrdatum.keys()) == False:
            self.usrdatum[userid] = {}
            for onekey in self.keyvectors.keys():
                self.usrdatum[userid][onekey] = 0;
                
        rst = []
        sortedtml = sorted(tmlscore.items(), key=itemgetter(1), reverse=True)
        
        maxcnt = 0
        for sortedkey in sortedtml:
            maxcnt += 1
            rst.append((sortedkey[0], sortedkey[1]))
            self.usrdatum[userid][sortedkey[0]] += sortedkey[1] * 3
            if maxcnt == 30 : break
        
        return rst
    
    def put_page_new(self, userid, pagetuplelist):
        pagescore = {}
        for pagetuple in pagetuplelist:
            for onekey in self.keyvectors.keys():
                if pagetuple[0] in self.model:
                    similar = self.model.similarity(pagetuple[0], onekey)
                    if(similar>0.5):
                        if onekey in pagescore.keys() : pagescore[onekey] += similar * pagetuple[1]
                        else : pagescore[onekey] = similar * pagetuple[1]
                            
        if (userid in self.usrdatum.keys()) == False:
            self.usrdatum[userid] = {}
            for onekey in self.keyvectors.keys():
                self.usrdatum[userid][onekey] = 0;
                
        rst = []
        sortedpage = sorted(pagescore.items(), key=itemgetter(1), reverse=True)
        maxcnt = 0
        for sortedkey in sortedpage:
            maxcnt += 1
            rst.append((sortedkey[0], sortedkey[1]))
            self.usrdatum[userid][sortedkey[0]] += sortedkey[1]
            if maxcnt == 30 : break
        
        return rst
    
    def put_page_exist(self, userid, pagetuplelist):
        if (userid in self.usrdatum) == False:
            self.usrdatum[userid] = {}
            for onekey in self.keyvectors.keys():
                self.usrdatum[userid][onekey] = 0;
                
        for ptuple in pagetuplelist:
            self.usrdatum[userid][ptuple[0]] += ptuple[1]
            
    def get_report(self):
        rpt = {}
        for oneusr in seusrdatum.keys():
            for onekey in iself.usrdatum[oneusr].keys():
                if (onekey in rpt.keys()) == False: rpt[onekey] = 0
                rpt[onekevy] += self.usrdatum[oneusr][onekey]
        
        sortedrpt = sorted(rpt.items(), key=itemgetter(1), reverse=True)
        return sortedrpt
            
    def clean(self):
        self.usrdatum = {}
