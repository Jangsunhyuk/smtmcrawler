import gensim
import math

from operator import itemgetter

class Keymatch:
    def __init__(self):
        #self.model = gensim.models.Word2Vec.load('lookalike_w2v_model')
        self.model = gensim.models.Word2Vec.load('src/keymatch/lookalike_namu_model3')
        #print('투자은행' in self.model)
        
        keylistfile = open('src/keymatch/keywords.txt', 'r', encoding='utf-8')
        
        self.keyvectors = {}
        while True:
            line = keylistfile.readline().replace("\n", "")
            if not line: break
            if line in self.model:
                self.keyvectors[line] = self.model[line]
                
        self.usrdatum = {}
                
    def put_user_timeline(self, userid, usrtuplelist):
        tmlscore = {}
        for usrtuple in usrtuplelist:
            for onekey in self.keyvectors.keys():
                if usrtuple[0] in self.model:
                    similar = self.model.similarity(usrtuple[0], onekey)
                    
                    if(similar>0.6):
                        #if onekey == '연애':
                            #print('연애 -- '+usrtuple[0]+' -- '+str(similar))
                        if onekey in tmlscore.keys() : 
                            tmlscore[onekey] += similar * usrtuple[1]
                        else :
                            tmlscore[onekey] = similar * usrtuple[1]
        
        #print(tmlscore)
        
        if (userid in self.usrdatum.keys()) == False:
            self.usrdatum[userid] = {}
            for onekey in self.keyvectors.keys():
                self.usrdatum[userid][onekey] = 0;
                
        rst = []
        sortedtml = sorted(tmlscore.items(), key=itemgetter(1), reverse=True)
        
        maxcnt = 0
        for sortedkey in sortedtml:
            maxcnt += 1
            score = math.sqrt(len(usrtuplelist)) * sortedkey[1] / sortedtml[0][1]
            rst.append((sortedkey[0], score))
            self.usrdatum[userid][sortedkey[0]] += score * 3
            if maxcnt == 30 : break
        
        return rst
    
    def put_page_new(self, userid, pagetuplelist):
        pagescore = {}
        for pagetuple in pagetuplelist:
            for onekey in self.keyvectors.keys():
                if pagetuple[0] in self.model:
                    similar = self.model.similarity(pagetuple[0], onekey)
                    if(similar>0.5):
                        if onekey in pagescore.keys() : pagescore[onekey] += similar * pagetuple[1]
                        else : pagescore[onekey] = similar * pagetuple[1]
                            
        if (userid in self.usrdatum.keys()) == False:
            self.usrdatum[userid] = {}
            for onekey in self.keyvectors.keys():
                self.usrdatum[userid][onekey] = 0;
                
        rst = []
        sortedpage = sorted(pagescore.items(), key=itemgetter(1), reverse=True)
        maxcnt = 0
        for sortedkey in sortedpage:
            maxcnt += 1
            rst.append((sortedkey[0], sortedkey[1]))
            self.usrdatum[userid][sortedkey[0]] += sortedkey[1]
            if maxcnt == 30 : break
        
        return rst
    
    def put_page_exist(self, userid, pagetuplelist):
        if (userid in self.usrdatum) == False:
            self.usrdatum[userid] = {}
            for onekey in self.keyvectors.keys():
                self.usrdatum[userid][onekey] = 0;
                
        for ptuple in pagetuplelist:
            self.usrdatum[userid][ptuple[0]] += ptuple[1]
            
    def get_report(self):
        rpt = {}
        for oneusr in self.usrdatum.keys():
            for onekey in self.usrdatum[oneusr].keys():
                #if (onekey in rpt.keys()) == False: rpt[onekey] = 0
                #rpt[onekey] += self.usrdatum[oneusr][onekey]
                if (onekey in rpt.keys()) == False: rpt[onekey] = [0,0]
                if self.usrdatum[oneusr][onekey] >= 0.0009:
                    rpt[onekey][0] += self.usrdatum[oneusr][onekey]
                    rpt[onekey][1] += 1
    
        #for onekey in rpt.keys():
            #if (rpt[onekey][1] / len(self.usrdatum.keys())) > 0.3:
            #    rpt[onekey] = rpt[onekey][0] / rpt[onekey][1]
            #else:
            #    rpt[onekey] = 0
                
            #if rpt[onekey][1] > 0:
            #    rpt[onekey] = rpt[onekey][0] / rpt[onekey][1]
           # else:
            #    rpt[onekey] = 0
        
        sortedrpt = sorted(rpt.items(), key=itemgetter(1), reverse=True)
        return sortedrpt
            
    def clean(self):
        self.usrdatum = {}
