from .text_rank import TextRank
import requests
import json

"""
	페이지/그룹 id 에 따라서 페이지/그룹을 TextRank 알고리즘으로 분석해 주고 결과를 출력한다.

	USAGE:

	pa = PageAnalysis()

	pa.page_analyze("244837599252239","group")
	pa.page_analyze("depromeet","page")
"""

class PageAnalysis:
	def __init__(self):
		client_id = "1038580469574247"
		client_secret = "30d64387b1404864c8ef5c923950d1d9"
		
		self.__get_fb_token(client_id,client_secret)

	#get facebook token
	def __get_fb_token(self,app_id, app_secret):
		payload = {'grant_type': 'client_credentials', 'client_id': app_id, 'client_secret': app_secret}
		file = requests.post('https://graph.facebook.com/oauth/access_token?', params = payload)
		#print file.text #to test what the FB api responded with    
		self.atkn = file.text.split("=")[1]

	#return : json dictionary response
	#warning : post_limit cannot exceed 100
	def __get_all_posts(self, url, post_limit = 15):
		_params = { 'limit' : post_limit, 'access_token' : self.atkn }
		try:
			file = requests.get(url,params = _params,timeout=5)
			dict = json.loads(file.text,encoding='utf-8')
			
			return dict
		#timeout Exception
		except Exception:
			return {}

	#atype : 페이지를 분석하려면 "page"를, 그룹을 분석하려면 "group"을 보낸다
	def page_analyze(self, pid, atype):
		print(pid+ " 의 페이지 분석 중 ... ")
		if atype == "page":
			url = "https://graph.facebook.com/" + pid + "/posts"
		elif atype == "group":
			url = "https://graph.facebook.com/" + pid + "/feed"

		dict = self.__get_all_posts(url,100)

		tr = TextRank()

		if "data" in dict:
			print("페이지 글을 가져오고 있습니다..")
			print("이 페이지에서 가져온 post 개수 : " + str(len(dict["data"])))

			for i in range(len(dict["data"])):
				if "message" in dict["data"][i]:
					if dict["data"][i]["message"]:
						data = dict["data"][i]["message"]

						tr.str_mapping_to_graph(data)
		else:
			print(pid + "은 분석할 수 없는 그룹/페이지 입니다")
			return None

		return tr.extract_keyword_from_graph(n=100)



